# Detection of road damage.

This repository is part of a larger project where we will test different types of ML to determine which ones can be effective to detect road damage from images. In this repository we use a pretrained model from sekilab, and testing it on our dataset. Our dataset is provided by Statens Vegvesen and contains images taken on Norwegian roads. The initial tests of this yielded that the model correctly classified cracks and damages on images taken directly towards the road. However, images taken infront of the car (with alot of disturbance, such as cars, houses, poels, trees etc.) failed.

In the folder "generated_results" you can see two sets of pictures. One set generated with the inception model, the other with the mobilenet model. Images should have corresponding numbering for easier comparison.

 This repository uses pretrained models from [github.com/sekilab/RoadDamageDetector](https://github.com/sekilab/RoadDamageDetector).

## Damage definitions.

![damagedef](./definitons/RoadDamageTypeDef.png)


## Comparison between inception and mobilenet.
There are many images that can be used as reference or comparison in ./generated_results/. However, here are a few comparisons of inception vs mobilenet.

### Inception
![Inception](./generated-results/inception/p18/inception_veidekke_generated_result42.jpg)
### Mobilenet
![Mobilenet](./generated-results/mobilenet/p18/mobilenet_generated_result42.jpg) 